#!/bin/bash

docker run --rm -it -p 5105:5105 --add-host=host.docker.internal:host-gateway gateway
docker run --rm -it -v /opt/indices/:/indices -p 5101:5101 --add-host=host.docker.internal:host-gateway --env CLUSTER_ID=1 search_index_service
docker run -d --rm -p 8501:8501 -v "/opt/models:/models" --env MODEL_NAME="model_use" tensorflow/serving

docker service create --replicas 1 --host host.docker.internal:host-gateway --name gateway --mount type=bind,src=/opt/indices/,dst=/indices -p 5105:5105 65.108.217.129:6000/gateway

docker service create --replicas 1 --name search_index_service_0 -p 5100:5101 --mount type=bind,src=/opt/indices/,dst=/indices --env CLUSTER_ID=0 65.108.217.129:6000/search_index_service

docker service create --replicas 1 --name search_index_service_1 -p 5101:5101 --mount type=bind,src=/opt/indices/,dst=/indices --env CLUSTER_ID=1 65.108.217.129:6000/search_index_service

docker service create --replicas 1 --name search_index_service_2 -p 5102:5101 --mount type=bind,src=/opt/indices/,dst=/indices --env CLUSTER_ID=2 65.108.217.129:6000/search_index_service

docker service create --replicas 1 --name search_index_service_3 -p 5103:5101 --mount type=bind,src=/opt/indices/,dst=/indices --constraint node.hostname==kcloud-production-user-220-vm-346 --env CLUSTER_ID=3 65.108.217.129:6000/search_index_service


docker service create --replicas 1 --name serving -p 8501:8501 --mount type=bind,src=/opt/models,dst=/models --env MODEL_NAME="model_use" tensorflow/serving


curl http://65.108.217.129:6000/v2/_catalog
