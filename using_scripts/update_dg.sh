#!/bin/bash
# Arguments:
# old dg
# new dg
# cluster 


for host in host_1 host_2; do
    # перемещение индексов старого поколения в архивный каталог
    rsync -a --progress --remove-source-files $host:/opt/indices/clust_$3 $host:/storage/dg_$1

    # перемещение индексов нового поколения в продакшн
    rsync -a --progress --remove-source-files $host:/storage/dg_$2/clust_$3 $host:/opt/indices/
    # обновления центроидов в продакшне для конкретного кластера

    ssh $host "python /opt/project/using_scripts/update_centroids.py --new_dg $2 --cluster_id $3 "
done

# rolling update сервиса кластера 
ssh host_2 "docker service update --force --update-parallelism 1 --update-delay 30s --update-failure-action rollback mlops_index_service_$3"

# rolling update сервиса маршрутизатора 
ssh host_2 "docker service update --force --update-parallelism 1 --update-delay 30s --update-failure-action rollback mlops_gateway"
