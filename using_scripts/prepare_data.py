import pickle
import numpy as np
import json
import faiss
import joblib
import os

for gen in range(1, 3):
    with open(f'clusters_use_dg{gen}.json') as f:
        clusters = json.load(f)
    os.makedirs(f'dgs/dg_{gen}', exist_ok=True)
    for clust_id in range(4):
        os.makedirs(f'dgs/dg_{gen}/clust_{clust_id}', exist_ok=True)
        with open(f'dgs/dg_{gen}/clust_{clust_id}/sentences.json', 'w') as f:
            json.dump(clusters[str(clust_id)], f)

for gen in range(1, 3):
    with open(f'clusters_use_dg{gen}.json') as f:
        clusters = json.load(f)
    with open(f'use_embeddings_dg{gen}.pkl', 'rb') as f:
        use_embeddings = pickle.load(f)
    for clust_id in range(4):
        embs_cluster = np.vstack(
            [use_embeddings[clusters[str(clust_id)][i]] for i in 
                range(len(clusters[str(clust_id)]))])
        index = faiss.IndexFlatL2(512)
        index.add(embs_cluster)
        joblib.dump(index, f'dgs/dg_{gen}/clust_{clust_id}/index')

for gen in range(1, 3):
    with open(f'clusters_centers_use_dg{gen}.pkl', 'rb') as f:
        clusters_centers = pickle.load(f)
        clust_centers_base = np.array([clusters_centers[str(i)] for i in range(4)])
        np.save(f'dgs/dg_{gen}/clust_centers', clust_centers_base)