# Создаём реджистри для образов docker и пушим в него docker образы

docker run -d -p 6000:5000 --restart=always --name registry registry:2

docker tag gateway 65.108.217.129:6000/base
docker push 65.108.217.129:6000/base

docker tag gateway 65.108.217.129:6000/gateway
docker push 65.108.217.129:6000/gateway

docker tag search_index_service 65.108.217.129:6000/search_index_service
docker push 65.108.217.129:6000/search_index_service