from flask import Flask, request, jsonify
import requests
import numpy as np
import json

clust_centers_base = np.load('/indices/clust_centers.npy')

app = Flask(__name__)

@app.route('/input', methods=['POST', 'GET'])
def get_data():
    data = request.get_data().decode('utf-8')
    response = requests.post(
        'http://host.docker.internal:8501/v1/models/model_use:predict', data).text
    input_emb = np.array(json.loads(response)['predictions'], dtype='float32')
    input_emb = input_emb[0, :]
    
    u_dot_v = np.sum(input_emb * clust_centers_base, axis=1)
    mod_u = np.sqrt(np.sum(input_emb * input_emb))
    mod_v = np.sqrt(np.sum(clust_centers_base * clust_centers_base, axis=1))
    cos_similar = u_dot_v / (mod_u * mod_v)
    input_cluster = np.argmin(cos_similar)
    resp = json.loads(
        requests.post(
        f'http://host.docker.internal:510{input_cluster}/get_k_neighbours',
        json={'emb':input_emb.tolist() }
    ).text)
    match = resp['best_match']
    cluster = resp['cluster_id']

    return jsonify(best_match=match, cluster=cluster)


